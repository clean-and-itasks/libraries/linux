implementation module System._Linux

import System._Pointer
import System.SysCall

sendfile :: !Int !Int !Pointer !Int !*env -> (!Int, !*env) | SysCallEnv env
sendfile out_fd in_fd offset count w = _sendfile out_fd in_fd offset count w
_sendfile :: !Int !Int !Pointer !Int !*env -> (!Int, !*env)
_sendfile out_fd in_fd offset count w = code {
		ccall sendfile "IIpI:I:A"
	}

ftruncate :: !Int !Int !*env -> (!Int, !*env) | SysCallEnv env
ftruncate fd length w = _ftruncate fd length w
_ftruncate :: !Int !Int !*env -> (!Int, !*env)
_ftruncate fd length w = code {
	ccall ftruncate "II:I:A"
}

prctl1 :: !Int !Int !*env -> *(!Int, !*env) | SysCallEnv env
prctl1 option arg2 w = _prctl1 option arg2 w
_prctl1 :: !Int !Int !*env -> *(!Int, !*env)
_prctl1 option arg2 w = code {
	ccall prctl "II:I:A"
}

get_nprocs :: !*env -> (!Int, !*env) | SysCallEnv env
get_nprocs env = _get_nprocs env
_get_nprocs :: !*env -> (!Int, !*env)
_get_nprocs env = code {
	ccall get_nprocs ":I:A"
}

stat :: !{#Char} !{#Char} !*env -> (!Int, !*env) | SysCallEnv env
stat path buf world = _stat path buf world
_stat :: !{#Char} !{#Char} !*env -> (!Int, !*env)
_stat path buf world = code {
	ccall stat "ss:I:A"
}

errno :: !*env -> (!Int, !*env) | SysCallEnv env
errno world = _errno world
_errno :: !*env -> (!Int, !*env)
_errno world = (readInt4S errnoAddr 0,world)
where
	errnoAddr :: Pointer
	errnoAddr = code {
		ccall __errno_location ":p"
	}
