definition module System._Linux

/**
 * Linux system calls and constants. Information about the meaning can be found
 * your system's manpages.
 */

from StdInt import IF_INT_64_OR_32
from System._Pointer import :: Pointer
from System.SysCall import class SysCallEnv

sendfile :: !Int !Int !Pointer !Int !*env -> (!Int, !*env) | SysCallEnv env special env=World

ftruncate :: !Int !Int !*env -> (!Int, !*env) | SysCallEnv env special env=World

DIRENT_D_NAME_OFFSET :== IF_INT_64_OR_32 19 11
PR_SET_PDEATHSIG :== 1
WNOHANG :== 0x00000001
WUNTRACED :== 0x00000002
MAXPATHLEN :== 1024

S_IFMT :== 0170000
S_IFIFO :== 0010000
S_IFCHR :== 0020000
S_IFDIR :== 0040000
S_IFBLK :== 0060000
S_IFREG :== 0100000
S_IFLNK :== 0120000
S_IFSOCK :== 0140000
S_IFWHT :== 0160000

STDIN_FILENO :== 0
STDOUT_FILENO :== 1
STDERR_FILENO :== 2

FIONREAD :== 0x541B

F_SETFD :== 2
FD_CLOEXEC :== 1
O_CLOEXEC :== 02000000
O_RDONLY :== 00
O_WRONLY :== 01
O_RDWR :== 02
O_CREAT :== 0100
O_NOCTTY :== 0400

TCSANOW :== 0
TIOCSCTTY :== 0x540E

ECHO :== 0x8
ECHONL :== 0x40
ICANON :== 0x2

//* Place an exclusive lock. Only one process may hold an exclusive lock for a given file at a given time.
LOCK_EX :== 2
//* Remove an existing lock held by this process.
LOCK_UN :== 8

//* Change the capacity of a pipe (see https://man7.org/linux/man-pages/man2/fcntl.2.html).
F_SETPIPE_SZ :== 1031

//* This is the `prctl1` version with one argument besides the `option`.
prctl1 :: !Int !Int !*env -> *(!Int, !*env) | SysCallEnv env special env=World

get_nprocs :: !*env -> (!Int, !*env) | SysCallEnv env special env=World

stat :: !{#Char} !{#Char} !*env -> (!Int, !*env) | SysCallEnv env special env=World
errno :: !*env -> (!Int, !*env) | SysCallEnv env special env=World
