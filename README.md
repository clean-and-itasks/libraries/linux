# linux

This library provides an interface to linux specific syscalls and constants.
For general posix syscalls see the `posix` package.

## Licence

The package is licensed under the BSD-2-Clause license; for details, see the
[LICENSE](/LICENSE) file.
